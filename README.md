﻿# Slic3r
============

# Load Config
![3D_photo_1](/config-slic3r/load_config.gif)

# License 
======

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

Attributions
============
See commit details to find the authors of each Part.
- @fandres323
